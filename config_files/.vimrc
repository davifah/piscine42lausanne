" project specific vim
set exrc
set secure

set nocompatible

filetype on

filetype plugin on

filetype indent on

set ruler

syntax on

set number

set cursorline

set shiftwidth=4

set tabstop=4

set incsearch

set ignorecase

set hlsearch

set wildmenu

set wildmode=list:longest

set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

call plug#begin('~/.vim/plugged')

	Plug 'alexandregv/norminette-vim'
	Plug 'dense-analysis/ale'
	Plug 'pbondoer/vim-42header'
"	Plug 'dracula/vim', { 'as': 'dracula' }
	Plug 'drewtempelmeyer/palenight.vim'
	Plug 'itchyny/lightline.vim'

call plug#end()

if (has("termguicolors"))
  set termguicolors
endif

let g:palenight_terminal_italics=1
let g:lightline = { 'colorscheme': 'palenight' }
set background=dark
colorscheme palenight
