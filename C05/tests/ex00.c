/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex00.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 15:37:21 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 15:46:03 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<math.h>
#include "../ex00/ft_iterative_factorial.c"

int	main(void)
{
	int	nb;

	nb = -1;
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_iterative_factorial(nb));
	nb = 0;
	printf("Expected : %d\n", 1);
	printf("Result   : %d\n", ft_iterative_factorial(nb));
	nb = 5;
	printf("Expected : %d\n", 120);
	printf("Result   : %d\n", ft_iterative_factorial(nb));
	nb = 11;
	printf("Expected : %d\n", 39916800);
	printf("Result   : %d\n", ft_iterative_factorial(nb));
}
