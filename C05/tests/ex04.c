/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 16:25:54 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 16:59:10 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex04/ft_fibonacci.c"

int	main(void)
{
	printf("Expected : %d\n", -1);
	printf("Result   : %d\n", ft_fibonacci(-1));
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_fibonacci(0));
	printf("Expected : %d\n", 1);
	printf("Result   : %d\n", ft_fibonacci(1));
	printf("Expected : %d\n", 1);
	printf("Result   : %d\n", ft_fibonacci(2));
	printf("Expected : %d\n", 89);
	printf("Result   : %d\n", ft_fibonacci(11));
	printf("Expected : %d\n", 987);
	printf("Result   : %d\n", ft_fibonacci(16));
}
