/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex06.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 18:06:16 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 18:28:28 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex06/ft_is_prime.c"

int	main(void)
{
	int	i;
	int	counter;

	i = -1;
	counter = 0;
	while(++i <= 100000)
	{
		if (ft_is_prime(i))
		{
			counter++;
		}
	}
	printf("Total prime numbers : %d\n", 9592);
	printf("Prime numbers found : %d\n", counter);
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_is_prime(0));
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_is_prime(1));
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_is_prime(-10));
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_is_prime(6));
	printf("Expected : %d\n", 1);
	printf("Result   : %d\n", ft_is_prime(7));
}
