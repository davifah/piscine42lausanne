/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex05.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 17:41:57 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 18:01:25 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<math.h>
#include "../ex05/ft_sqrt.c"

int	main(void)
{
	int	n;

	n = 1;
	printf("Expected : %d\n", (int)sqrt(n));
	printf("Result   : %d\n", ft_sqrt(n));
	n = -5;
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_sqrt(n));
	n = 0;
	printf("Expected : %d\n", (int)sqrt(n));
	printf("Result   : %d\n", ft_sqrt(n));
	n = 49;
	printf("Expected : %d\n", (int)sqrt(n));
	printf("Result   : %d\n", ft_sqrt(n));
	n = 214710409;
	printf("Expected : %d\n", (int)sqrt(n));
	printf("Result   : %d\n", ft_sqrt(n));
	n = 2147483647;
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_sqrt(n));
}
