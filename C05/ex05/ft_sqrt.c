/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 17:35:00 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 17:58:37 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

long	ft_square(unsigned long nb);

int	ft_sqrt(int nb)
{
	int	i;

	i = -1;
	while (ft_square(++i) <= (long) nb)
		;
	return ((i - 1) * (ft_square(i - 1) == nb));
}

long	ft_square(unsigned long nb)
{
	return (nb * nb);
}
