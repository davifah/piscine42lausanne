/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/07 15:53:37 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/08 17:55:32 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	ft_char_upcase(char c);

int		ft_is_alphanumeric(char c);

char	*ft_strcapitalize(char *str)
{
	int	i;

	i = -1;
	while (str[++i] != '\0')
	{
		str[i] += ('a' - 'A') * ('A' <= str[i] && str[i] <= 'Z');
		if (!ft_is_alphanumeric(str[i - 1]) || i == 0)
			str[i] = ft_char_upcase(str[i]);
	}
	return (str);
}

char	ft_char_upcase(char c)
{
	return (c + (('A' - 'a') * ('a' <= c && c <= 'z')));
}

int	ft_is_alphanumeric(char c)
{
	int	res;

	res = 0;
	res += ('0' <= c && c <= '9');
	res += ('a' <= c && c <= 'z');
	res += ('A' <= c && c <= 'Z');
	return (res);
}
