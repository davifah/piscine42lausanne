/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex09.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/07 16:20:38 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/09 17:22:14 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex09/ft_strcapitalize.c"

int	main(void)
{
	char	c[] = "salut, comment tu vas ? 42mots quarante-deux; cinquante+et+un aaAAA";

	printf("Expected : Salut, Comment Tu Vas ? 42mots Quarante-Deux; Cinquante+Et+Un Aaaaa\n");
	printf("Result   : %s\n", ft_strcapitalize(c));
}
