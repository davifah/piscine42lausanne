/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 14:20:01 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/09 16:30:53 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex01/ft_strncpy.c"

int	main(void)
{
	char	c[50];

	printf("Expected : 01234\n");
	ft_strncpy(c, "0123456789", 5);
	printf("Result   : %s\n", c);
	printf("Expected : 012\n");
	ft_strncpy(c, "012\0i345", 7);
	printf("Result   : %s\n", c);
	ft_strncpy(c, "01234567890123456789", 20);
	printf("Expected : 01234567890123456789\n");
	ft_strncpy(c, "0123456789", 10);
	printf("Result   : %s\n", c);
}
