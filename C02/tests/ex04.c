/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 14:46:05 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/06 15:12:58 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex04/ft_str_is_lowercase.c"

int	main(void)
{
	printf("Expected : 1 , Result : %d\n", ft_str_is_lowercase("loweraz"));
	printf("Expected : 1 , Result : %d\n", ft_str_is_lowercase(""));
	printf("Expected : 0 , Result : %d\n", ft_str_is_lowercase("0123456789"));
	printf("Expected : 0 , Result : %d\n", ft_str_is_lowercase(" "));
	printf("Expected : 0 , Result : %d\n", ft_str_is_lowercase("/"));
	printf("Expected : 0 , Result : %d\n", ft_str_is_lowercase(":"));
	printf("Expected : 0 , Result : %d\n", ft_str_is_lowercase("ABCDEFZ"));
}
