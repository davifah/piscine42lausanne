/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex10.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/08 10:26:15 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/08 11:08:12 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex10/ft_strlcpy.c"

int	main(void)
{
	char	c0[8];
	int		n;
	n = ft_strlcpy(c0, "012345", 6);
	printf("%s - %d\n", c0, n);
	n = ft_strlcpy(c0, "0123456", 6);
	printf("%s - %d\n", c0, n);
	n = ft_strlcpy(c0, "0123456789", 10);
	printf("%s - %d\n", c0, n);
	n = ft_strlcpy(c0, "0123456", 3);
	printf("%s - %d\n", c0, n);
	n = ft_strlcpy(c0, "0123456", 0);
	printf("%s - %d\n", c0, n);
}
