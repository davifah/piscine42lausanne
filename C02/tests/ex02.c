/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex02.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 14:46:05 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/06 15:08:14 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex02/ft_str_is_alpha.c"

int	main(void)
{
	printf("Expected : 1 , Result : %d\n", ft_str_is_alpha("UPPERlowerAZaz"));
	printf("Expected : 1 , Result : %d\n", ft_str_is_alpha(""));
	printf("Expected : 0 , Result : %d\n", ft_str_is_alpha("1234"));
	printf("Expected : 0 , Result : %d\n", ft_str_is_alpha(" "));
	printf("Expected : 0 , Result : %d\n", ft_str_is_alpha("@"));
	printf("Expected : 0 , Result : %d\n", ft_str_is_alpha("["));
}
