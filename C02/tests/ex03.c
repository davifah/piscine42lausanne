/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex03.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 14:46:05 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/06 15:07:56 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex03/ft_str_is_numeric.c"

int	main(void)
{
	printf("Expected : 0 , Result : %d\n", ft_str_is_numeric("UPPERlower"));
	printf("Expected : 1 , Result : %d\n", ft_str_is_numeric(""));
	printf("Expected : 1 , Result : %d\n", ft_str_is_numeric("0123456789"));
	printf("Expected : 0 , Result : %d\n", ft_str_is_numeric(" "));
	printf("Expected : 0 , Result : %d\n", ft_str_is_numeric("/"));
	printf("Expected : 0 , Result : %d\n", ft_str_is_numeric(":"));
}
