/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex06.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 14:46:05 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/07 14:33:48 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex06/ft_str_is_printable.c"

int	main(void)
{
	char	str[] = {127, 31};
	printf("Expected : 1 , Result : %d\n", ft_str_is_printable("loweraz"));
	printf("Expected : 1 , Result : %d\n", ft_str_is_printable(""));
	printf("Expected : 1 , Result : %d\n", ft_str_is_printable("0123456789"));
	printf("Expected : 1 , Result : %d\n", ft_str_is_printable(" ~"));
	printf("Expected : 0 , Result : %d\n", ft_str_is_printable(str));
}
