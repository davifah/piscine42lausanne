/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex05.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/06 14:46:05 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/07 14:17:13 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex05/ft_str_is_uppercase.c"

int	main(void)
{
	printf("Expected : 0 , Result : %d\n", ft_str_is_uppercase("loweraz"));
	printf("Expected : 1 , Result : %d\n", ft_str_is_uppercase(""));
	printf("Expected : 0 , Result : %d\n", ft_str_is_uppercase("0123456789"));
	printf("Expected : 0 , Result : %d\n", ft_str_is_uppercase(" "));
	printf("Expected : 0 , Result : %d\n", ft_str_is_uppercase("/"));
	printf("Expected : 0 , Result : %d\n", ft_str_is_uppercase(":"));
	printf("Expected : 1 , Result : %d\n", ft_str_is_uppercase("ABCDEFZ"));
}
