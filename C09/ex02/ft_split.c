/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/18 15:41:58 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/18 16:37:17 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdlib.h>

int				is_in_str(char c, char *str);

unsigned int	count_sep(char *str, char *charset);

char			*assign_str(char *str, char *charset, int start);

char	**ft_split(char *str, char *charset)
{
	char			**tab;
	char			prev;
	unsigned int	len;
	int				i;
	int				j;

	tab = malloc((sizeof(char *) * count_sep(str, charset)) + 1);
	if (tab == NULL)
		return (NULL);
	i = -1;
	j = 0;
	prev = 0;
	while (str[++i])
	{
		len = 0;
		if ((is_in_str(prev, charset) || !prev) && !is_in_str(str[i], charset))
			tab[j++] = assign_str(str, charset, i);
		prev = str[i];
	}
	tab[j] = 0;
	return (tab);
}

char	*assign_str(char *str, char *charset, int start)
{
	int		i;
	int		j;
	char	*re;

	i = 0;
	while (!is_in_str(str[i + start], charset) && str[i + start])
		i++;
	re = malloc(sizeof(char) * i);
	if (!re)
		return (NULL);
	j = -1;
	i = -1;
	while (!is_in_str(str[++i + start], charset) && str[i + start])
		re[i] = str[i + start];
	return (re);
}

unsigned int	count_sep(char *str, char *charset)
{
	char	prev;
	int		counter;
	int		i;	

	i = -1;
	counter = 0;
	prev = 0;
	while (str[++i])
	{
		if ((is_in_str(prev, charset) || !prev) && !is_in_str(str[i], charset))
			counter++;
		prev = str[i];
	}
	return (counter);
}

int	is_in_str(char c, char *str)
{
	while (*str)
	{
		if (c == *str++)
			return (1);
	}
	return (0);
}
