/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/17 11:00:04 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/17 13:25:28 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdlib.h>

char	*ft_strcat(char *dest, char *src);

int		ft_strlen(char *str);

char	*ft_strjoin(int size, char **strs, char *sep)
{
	char	*res;
	int		res_size;
	int		is_first;
	int		i;

	res_size = size * ft_strlen(sep);
	i = -1;
	while (++i < size)
		res_size += ft_strlen(strs[i]);
	if (size < 0)
		res = malloc(0);
	else
		res = malloc(res_size * sizeof(char));
	is_first = 1;
	i = -1;
	while (++i < size)
	{
		if (!is_first)
			ft_strcat(res, sep);
		is_first = 0;
		ft_strcat(res, strs[i]);
	}
	return (res);
}

char	*ft_strcat(char *dest, char *src)
{
	int	i;
	int	dest_size;
	int	is_done;

	i = -1;
	dest_size = ft_strlen(dest) - 1;
	is_done = 0;
	while (!is_done)
	{
		if (!src[++i])
			is_done = 1;
		dest[++dest_size] = src[i];
	}
	return (dest);
}

int	ft_strlen(char *str)
{
	int	counter;

	counter = -1;
	while (str[++counter] != '\0')
		continue ;
	return (counter);
}
