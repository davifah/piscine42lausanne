/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/13 11:41:19 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/17 10:09:26 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex01/ft_range.c"

void	print_int_array(int n[], int size);

int	main(void)
{
	int min = 2147483640;
	int max = 2147483647;
	printf("Min - included : %d\n", min);
	printf("Max - excluded : %d\n", max);
	print_int_array(ft_range(min, max), max - min);
	min = 10;
	max = 5;
	printf("Min - included : %d\n", min);
	printf("Max - excluded : %d\n", max);
	printf("%p\n", ft_range(min, max));
}

void	print_int_array(int n[], int size)
{
	int i;

	i = -1;
	while (++i < size)
		printf("%d , ", n[i]);
	printf("\nend - %d\n", n[i]);
}
