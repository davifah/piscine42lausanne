/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex00.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/13 11:18:14 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/13 11:29:13 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>
#include "../ex00/ft_strdup.c"

char	*ft1_strcpy(char *dest, char *src);

int	main(void)
{
	char	str[100];

	ft1_strcpy(str, "0123456789");
	printf("Expected : %s\n", strdup(str));
	printf("Result   : %s\n", ft_strdup(str));
	ft1_strcpy(str, "");
	printf("Expected : %s\n", strdup(str));
	printf("Result   : %s\n", ft_strdup(str));
	ft1_strcpy(str, "abcdefg/\0000000000");
	printf("Expected : %s\n", strdup(str));
	printf("Result   : %s\n", ft_strdup(str));
}

char	*ft1_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
