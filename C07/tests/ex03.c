/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex03.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/17 11:11:09 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/17 13:28:18 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<stdlib.h>
#include "../ex03/ft_strjoin.c"

char	*ft_strcpy(char *dest, char *src);

int	main(void)
{
	char	**strs;
	char	*res;

	strs = malloc(sizeof(char *) * 4);
	strs[0] = malloc(sizeof(char) * 4);
	strs[1] = malloc(sizeof(char) * 2);
	strs[2] = malloc(sizeof(char) * 1);
	strs[3] = malloc(sizeof(char) * 4);
	ft_strcpy(strs[0], "This");
	ft_strcpy(strs[1], "is");
	ft_strcpy(strs[2], "a");
	ft_strcpy(strs[3], "test");
	res = ft_strjoin(4, strs, " ");
	printf("%s - length %d\n", res, ft_strlen(res));
	res = ft_strjoin(4, strs, " , ");
	printf("%s - length %d\n", res, ft_strlen(res));
	res = ft_strjoin(0, strs, " , ");
	free(res);
	res = ft_strjoin(-400000, strs, " ,     ");
	free(res);
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
