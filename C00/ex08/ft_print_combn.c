/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_combn.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/05 10:56:24 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/05 11:31:05 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

void	ft_putchar(char c);

void	ft_print_recursiven(int n, int this_n);

void	ft_print_combn(int n)
{
	int	this_n;

	this_n = 0;
	if (n <= 0 || n >= 10)
		return ;
	ft_print_recursiven(n, this_n);
}
