/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/04 14:51:36 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/05 10:43:40 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

void	ft_print_pairs(int n0, int n1);

void	ft_putchar(char c);

void	ft_print_comb2(void)
{
	int	n0;
	int	n1;
	int	is_first;

	n0 = 0;
	is_first = 1;
	while (n0 <= 99)
	{
		n1 = n0 + 1;
		while (n1 <= 99)
		{
			if (!is_first)
				write(1, ", ", 2);
			else
				is_first = 0;
			ft_print_pairs(n0, n1);
			n1++;
		}
		n0++;
	}
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_print_pairs(int n0, int n1)
{
	char	c0;
	char	c1;
	char	c2;
	char	c3;

	c0 = ((int) n0 / 10) + '0';
	c1 = (n0 % 10) + '0';
	c2 = ((int) n1 / 10) + '0';
	c3 = (n1 % 10) + '0';
	ft_putchar(c0);
	ft_putchar(c1);
	ft_putchar(' ');
	ft_putchar(c2);
	ft_putchar(c3);
}
