/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/10 11:10:44 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/10 18:22:25 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	restart(int *i, int *start);

char	*return_value(char *str, char *to_find, int start, int j);

char	*ft_strstr(char *str, char *to_find)
{
	int		i;
	int		j;
	int		start;

	i = -1;
	j = -1;
	start = -1;
	while (str[++i])
	{
		if (!to_find[j + 1] && !(start < 0))
			return (return_value(str, to_find, start, j));
		else if (str[i] != to_find[j + 1])
			restart(&j, &start);
		if (str[i] == to_find[++j])
		{
			if (start < 0)
				start = i;
		}
		else
			restart(&j, &start);
	}
	return (return_value(str, to_find, start, j));
}

char	*return_value(char *str, char *to_find, int start, int j)
{
	if (!to_find[j + 1] && !(start < 0))
		return (&str[start]);
	if (!to_find[0])
		return (str);
	return (0);
}

void	restart(int *i, int *start)
{
	*i = -1;
	*start = -1;
}
