/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex02.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/10 10:14:39 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/10 10:53:27 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>
#include "../ex02/ft_strcat.c"

char	*ft_strcpy(char *dest, char *src);

int	main(void)
{
	char	c1[100];
	char	c2[100];

	ft_strcpy(c1, "0123");
	ft_strcpy(c2, "45\0xadsa");
	printf("Expected : %s\n", strcat(c1, c2));
	ft_strcpy(c1, "0123");
	printf("Result   : %s\n", ft_strcat(c1, c2));
	ft_strcpy(c1, "0123");
	ft_strcpy(c2, "456789");
	printf("Expected : %s\n", strcat(c1, c2));
	ft_strcpy(c1, "0123");
	printf("Result   : %s\n", ft_strcat(c1, c2));
	ft_strcpy(c1, "012\0x23");
	ft_strcpy(c2, "3456789");
	printf("Expected : %s\n", strcat(c1, c2));
	ft_strcpy(c1, "012\0x23");
	printf("Result   : %s\n", ft_strcat(c1, c2));
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
