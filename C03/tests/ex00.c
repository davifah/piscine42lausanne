/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex00.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/09 14:25:33 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/10 09:51:51 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>
#include "../ex00/ft_strcmp.c"

char	*ft_strcpy(char *dest, char *src);

int	main(void)
{
	char	c1[100];
	char	c2[100];

	ft_strcpy(c1, "This is a test");
	ft_strcpy(c2, "This is a test");
	printf("Expected : %d\n", strcmp(c1, c2));
	printf("Result   : %d\n", ft_strcmp(c1, c2));
	ft_strcpy(c1, "0123");
	ft_strcpy(c2, "012");
	printf("Expected : %d\n", strcmp(c1, c2));
	printf("Result   : %d\n", ft_strcmp(c1, c2));
	ft_strcpy(c1, "0123");
	ft_strcpy(c2, "0129");
	printf("Expected : %d\n", strcmp(c1, c2));
	printf("Result   : %d\n", ft_strcmp(c1, c2));
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
