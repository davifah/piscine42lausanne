/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/09 14:25:33 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/10 10:00:03 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>
#include "../ex01/ft_strncmp.c"

char	*ft_strcpy(char *dest, char *src);

int	main(void)
{
	char	c1[100];
	char	c2[100];

	ft_strcpy(c1, "This is a test");
	ft_strcpy(c2, "This is a test");
	printf("Expected : %d\n", strncmp(c1, c2, 6));
	printf("Result   : %d\n", ft_strncmp(c1, c2, 6));
	ft_strcpy(c1, "0123");
	ft_strcpy(c2, "012");
	printf("Expected : %d\n", strncmp(c1, c2, 5));
	printf("Result   : %d\n", ft_strncmp(c1, c2, 5));
	ft_strcpy(c1, "0123456");
	ft_strcpy(c2, "0129456");
	printf("Expected : %d\n", strncmp(c1, c2, 4));
	printf("Result   : %d\n", ft_strncmp(c1, c2, 4));
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
