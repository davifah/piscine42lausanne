/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/10 11:25:02 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/10 12:07:21 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>
#include "../ex04/ft_strstr.c"

char	*ft_strcpy(char *dest, char *src);

int	main(void)
{
	char	c1[100];
	char	c2[100];

	ft_strcpy(c1, "01234567890123456789");
	ft_strcpy(c2, "");
	printf("Expected : %s\n", strstr(c1, c2));
	printf("Result   : %s\n", ft_strstr(c1, c2));
	ft_strcpy(c2, "2345");
	printf("Expected : %s\n", strstr(c1, c2));
	printf("Result   : %s\n", ft_strstr(c1, c2));
	ft_strcpy(c2, "999");
	printf("Expected : %s\n", strstr(c1, c2));
	printf("Result   : %s\n", ft_strstr(c1, c2));
	ft_strcpy(c1, "abcdeeebbasjdhkahw");
	ft_strcpy(c2, "ba");
	printf("Expected : %s\n", strstr(c1, c2));
	printf("Result   : %s\n", ft_strstr(c1, c2));
	ft_strcpy(c1, "abcdeeebbastest");
	ft_strcpy(c2, "ebbastest\0");
	printf("Expected : %s\n", strstr(c1, c2));
	printf("Result   : %s\n", ft_strstr(c1, c2));
	ft_strcpy(c1, "abcdeeebbastest");
	ft_strcpy(c2, "eb");
	printf("Expected : %s\n", strstr(c1, c2));
	printf("Result   : %s\n", ft_strstr(c1, c2));
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
