/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strs_to_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/17 15:39:06 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/17 17:56:53 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdlib.h>
#include "ft_stock_str.h"

char	*ft_strcpy(char *dest, char *src);

int		ft_strlen(char *str);

struct s_stock_str	*ft_strs_to_tab(int ac, char **av)
{
	t_stock_str	*tab;
	t_stock_str	*temp;
	int			i;

	tab = malloc((ac * sizeof(t_stock_str)) + 1);
	if (tab == NULL || ac < 0)
		return (0);
	i = -1;
	while (++i < ac)
	{
		temp = (t_stock_str *)malloc(sizeof(t_stock_str));
		if (temp == 0)
			return (0);
		tab[i] = *temp;
		tab[i].size = ft_strlen(av[i]);
		tab[i].str = av[i];
		tab[i].copy = malloc(ft_strlen(av[i]));
		if (tab[i].copy)
			ft_strcpy(tab[i].copy, av[i]);
	}
	tab[i].str = 0;
	return (tab);
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}

int	ft_strlen(char *str)
{
	int	counter;

	counter = -1;
	while (str[++counter] != '\0')
		continue ;
	return (counter);
}
