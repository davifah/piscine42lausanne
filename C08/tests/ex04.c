/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/17 15:37:38 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/17 17:53:52 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "../ex04/ft_strs_to_tab.c"
#include "../ex05/ft_show_tab.c"

char	*ft1_strcpy(char *dest, char *src);

int	main(void)
{
	char		**strs;
	char		n;
	t_stock_str	*tab;

	tab = 0;
	n = 4;
	strs = malloc(n * sizeof(char *));
	strs[0] = malloc (1 * sizeof(char));
	strs[1] = malloc (3 * sizeof(char));
	strs[2] = malloc (10 * sizeof(char));
	strs[3] = malloc (10 * sizeof(char));
	ft1_strcpy(strs[0], "1");
	ft1_strcpy(strs[1], "234");
	ft1_strcpy(strs[2], "0123456789");
	ft1_strcpy(strs[3], "unmodified");

	tab = ft_strs_to_tab(n, strs);
	if (!tab)
		printf("TAB IS NULL\n");
	tab[3].copy = "modified";
	ft_show_tab(tab);
}

char	*ft1_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
