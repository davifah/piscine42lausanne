/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex05.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 11:19:40 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 14:01:02 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<stdlib.h>
#include "../ex05/ft_atoi_base.c"

char	*ft_strcpy(char *dest, char *src);

int	main(void)
{
	char	c[100];
	char	base[] = "0123456789";

	ft_strcpy(c, "-2147483648abcdsfafeadgsfdfawda");
	printf("Expected : %d\n", atoi(c));
	printf("Result   : %d\n", ft_atoi_base(c, base));
	ft_strcpy(c, " \f\n\r\t\v----2147483647abcdsfafeadgsfdfawda");
	printf("Expected : %d\n", 2147483647);
	printf("Result   : %d\n", ft_atoi_base(c, base));
	ft_strcpy(c, " \f\n\r\t\v-1--33bcdsfax-1998dfawda");
	printf("Expected : %d\n", -1);
	printf("Result   : %d\n", ft_atoi_base(c, base));
	ft_strcpy(c, " -\f\n\r\t\v-1--33bcdsfax-1998dfawda");
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_atoi_base(c, base));
	ft_strcpy(c, " \f\n\r\t\v---bcdsfax-1998dfawda");
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_atoi_base(c, base));
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
