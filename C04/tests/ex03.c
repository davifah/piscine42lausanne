/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex03.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 11:19:40 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 17:21:26 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<stdlib.h>
#include "../ex03/ft_atoi.c"

char	*ft_strcpy(char *dest, char *src);

int	main(void)
{
	char	c[100];

	ft_strcpy(c, "-2147483648abcdsfafeadgsfdfawda");
	printf("Expected : %d\n", atoi(c));
	printf("Result   : %d\n", ft_atoi(c));
	ft_strcpy(c, " \f\n\r\t\v----2147483647abcdsfafeadgsfdfawda");
	printf("Expected : %d\n", 2147483647);
	printf("Result   : %d\n", ft_atoi(c));
	ft_strcpy(c, " \f\n\r\t\v-1--33bcdsfax-1998dfawda");
	printf("Expected : %d\n", -1);
	printf("Result   : %d\n", ft_atoi(c));
	ft_strcpy(c, " \f\n\r\t\v  ---+--+-12a++-34ab567");
	printf("Expected : %d\n", 12);
	printf("Result   : %d\n", ft_atoi(c));
	ft_strcpy(c, " \f\n\r\t\v-++--21583132abcdsfafeadgsfdfawda");
	printf("Expected : %d\n", -21583132);
	printf("Result   : %d\n", ft_atoi(c));
	ft_strcpy(c, " -\f\n\r\t\v-1--33bcdsfax-1998dfawda");
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_atoi(c));
	ft_strcpy(c, " \f\n\r\t\v---bcdsfax-1998dfawda");
	printf("Expected : %d\n", 0);
	printf("Result   : %d\n", ft_atoi(c));
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
