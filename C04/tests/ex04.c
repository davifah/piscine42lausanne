/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 12:21:35 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 17:29:27 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include "../ex04/ft_putnbr_base.c"

char	*ft_strcpy(char *dest, char *src);

int	main(void)
{
	printf("%s - Correct\n", "222224");
	ft_putnbr_base(62, "42");
	printf("\n%s - Correct\n", "971326846");
	ft_putnbr_base(971326846, "0123456789");
	printf("\n%s - Correct\n", "ff");
	ft_putnbr_base(255, "0123456789abcdef");
	printf("\n%s - Correct\n", "-DAVI");
	ft_putnbr_base(-194, "0DAVI");
	printf("\n%s - Correct\n", "-2147483648");
	ft_putnbr_base(-2147483648, "0123456789");
	printf("\nErrors :");
	ft_putnbr_base(255, "");
	ft_putnbr_base(255, " ");
	ft_putnbr_base(255, "0158221");
	ft_putnbr_base(255, "13547+");
}

char	*ft_strcpy(char *dest, char *src)
{
	int	i;
	int	is_done;

	i = 0;
	is_done = 0;
	while (!is_done)
	{
		dest[i] = src[i];
		if (src[i++] == '\0')
			is_done = 1;
	}
	return (dest);
}
