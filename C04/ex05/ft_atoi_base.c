/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 11:02:27 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 14:27:19 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>

int	get_int_start(char *str, int *negative, char *base);

int	handle_conditions(char c, int re, int had_negative, char *base);

int	ft_atoi_base(char *str, char *base)
{
	int				i;
	int				negative;
	int				level;
	unsigned int	n;

	negative = 1;
	level = 1;
	n = 0;
	i = get_int_start(str, &negative, base) + 1;
	while (handle_conditions(str[++i], 0, 0, base))
	{
		n += (str[i] - '0') * level;
		level *= 10;
	}
	return (n * negative);
}

int	get_int_start(char *str, int *negative, char *base)
{
	int	i;
	int	re;
	int	had_negative;
	int	is_done;

	i = -1;
	re = -1;
	had_negative = 0;
	is_done = 0;
	while (!is_done)
	{
		if (handle_conditions(str[++i], re, had_negative, ""))
			;
		else if (str[i] == '-' && re < 0)
		{
			*negative *= -1;
			had_negative = 1;
		}
		else if (handle_conditions(str[i], 0, 0, base))
			re = i;
		else
			is_done = 1;
	}
	return (re);
}

int	handle_conditions(char c, int re, int had_negative, char *base)
{
	int	res;
	int	i;

	res = 0;
	i = -1;
	if (base[0])
	{
		while (base[++i])
			if (base[i] == c)
				res += 1;
		return (res);
	}
	res = (9 <= c && c <= 13) || c == ' ';
	res *= re < 0;
	res *= !had_negative;
	printf("%d - %d - %d - %d\n", c, re, had_negative, res);
	return (res);
}
