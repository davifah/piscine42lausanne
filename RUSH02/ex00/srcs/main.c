#include "ft.h"
#include "nbr_to_word.h"
#include "load_dict.h"

int	main(int argc, char **argv)
{
	long	nbr;
	int		index;

	index = 1;
	if (argc >= 2 && argc <= 3)
	{	
		if (argc == 3)
			index = 2;
		if (!ft_str_is_numeric(argv[index]))
			return (ft_error(ERROR));
		nbr = ft_atoi(argv[index]);
		if (nbr < 0)
			return (ft_error(ERROR));
		if (argc == 3)
			print_nbr_to_word(nbr, argv[1]);
		else
			print_nbr_to_word(nbr, DEFAULT_DICT);
	}
	else
		return (ft_error(ERROR));
	return (0);
}
