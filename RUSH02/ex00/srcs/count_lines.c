#include "load_dict.h"

int	count_line(char *str)
{
	int	i;
	int	line_count;

	i = -1;
	line_count = 0;
	while (str[++i])
	{
		if ((!i && str[i] != '\n') || (str[i] != '\n' && str[i - 1] == '\n'))
			line_count++;
	}
	return (line_count);
}
