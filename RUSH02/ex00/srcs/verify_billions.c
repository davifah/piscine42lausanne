#include "ft.h"
#include "nbr_to_word.h"
#include "load_dict.h"

void	verify_billions(unsigned int nbr, t_dict *dict, int *error)
{
	verify_hundreds(nbr / 1000000000, dict, error);
	if (find_in_dict(1000000000, dict) < 0)
		(*error)++;
	if (nbr % 1000000000 > 0)
		verify_millions(nbr % 1000000000, dict, error);
}
