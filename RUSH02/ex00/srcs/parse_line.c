#include "load_dict.h"
#include "ft.h"

int	parse_nbr(char *str, t_dict *dict, int write);
int	get_str_start(char *str);

int	parse_line(char *str, t_dict *dict)
{
	int		i;
	int		j;
	char	prev;

	if (parse_nbr(str, dict, 1) < 0)
		return (parse_nbr(str, dict, 0));
	i = get_str_start(str);
	if (i < 0)
		return (-1);
	j = -1;
	prev = ' ';
	while (str[++i] != '\n' && str[i] != '\0')
	{
		if (!(prev == ' ' && str[i] == ' ') && str[i] != ' ')
		{
			if (ft_strlen(dict->str) > 0 && str[i] != ' ' && prev == ' ')
				dict->str[++j] = ' ';
			dict->str[++j] = str[i];
		}
		prev = str[i];
	}
	return (0 - (!ft_strlen(dict->str)));
}

int	get_str_start(char *str)
{
	int	i;
	int	started;

	started = 0;
	i = -1;
	while (str[++i] != '\n' && str[i] != '\0')
	{
		if (!started && !('0' <= str[i] && str[i] <= '9'))
			started = 1;
		if (started)
		{
			if (str[i] == ':')
				return (i);
			else if (str[i] != ' ')
				return (-1);
		}
	}
	return (-1);
}

int	parse_nbr(char *str, t_dict *dict, int write)
{
	long	atoi_ret;

	atoi_ret = ft_atoi(str);
	if (atoi_ret < 0)
		return (atoi_ret);
	if (write)
		dict->nbr = atoi_ret;
	return (0);
}
