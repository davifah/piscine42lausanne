#include "load_dict.h"

t_dict	*load_dict(char *filename)
{
	t_dict	*dict_list;
	char	*str;

	str = file_to_str(filename);
	if (!str)
		return (0);
	dict_list = populate_dict(str);
	free(str);
	return (verify_repetition(verify_strings(dict_list)));
}

t_dict	*verify_strings(t_dict *dict_list)
{
	int	i;

	if (!dict_list)
		return (0);
	i = -1;
	while (dict_list[++i].str)
	{
		if (!ft_str_is_printable(dict_list[i].str))
			return (0);
	}
	return (dict_list);
}

t_dict	*verify_repetition(t_dict *dict_list)
{
	int	i;
	int	j;

	if (!dict_list)
		return (0);
	i = -1;
	while (dict_list[++i].str)
	{
		j = -1;
		while (dict_list[++j].str)
		{
			if (i != j && dict_list[i].nbr == dict_list[j].nbr)
				return (NULL);
		}
	}
	return (dict_list);
}
