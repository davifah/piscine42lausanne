#include "load_dict.h"

int	find_in_dict(unsigned int nbr, t_dict *dict)
{
	unsigned int	i;

	i = 0;
	while (dict[i].str != 0)
	{
		if (dict[i].nbr == nbr)
			return (i);
		i++;
	}
	return (-1);
}
