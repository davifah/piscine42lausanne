#include "load_dict.h"
#include "nbr_to_word.h"

int	verify_dict(unsigned int nbr, t_dict *dict)
{
	int	error;

	error = 0;
	if (nbr == 0)
		if (find_in_dict(0, dict) < 0)
			error++;
	if (nbr <= 999)
		verify_hundreds(nbr, dict, &error);
	if (nbr > 999 && nbr <= 999999)
		verify_thousands(nbr, dict, &error);
	if (nbr > 999999 && nbr <= 999999999)
		verify_millions(nbr, dict, &error);
	if (nbr > 999999999)
		verify_billions(nbr, dict, &error);
	return (error);
}
