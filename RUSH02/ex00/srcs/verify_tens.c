#include "ft.h"
#include "load_dict.h"
#include "nbr_to_word.h"

void	verify_tens(unsigned int nbr, t_dict *dict, int *error)
{
	if (nbr <= 19)
	{
		if (find_in_dict(nbr, dict) < 0)
			(*error)++;
	}
	else
	{
		if (find_in_dict((nbr / 10) * 10, dict) < 0)
			(*error)++;
		if (nbr % 10 > 0)
			if (find_in_dict(nbr % 10, dict) < 0)
				(*error)++;
	}
}
