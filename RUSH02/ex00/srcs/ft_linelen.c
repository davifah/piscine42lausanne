#include "load_dict.h"

int	ft_linelen(char *str)
{
	int	counter;

	counter = -1;
	while (str[++counter] != '\0' && str[counter] != '\n')
		continue ;
	return (counter);
}
