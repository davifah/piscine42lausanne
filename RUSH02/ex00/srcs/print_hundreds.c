#include "ft.h"
#include "load_dict.h"
#include "nbr_to_word.h"

void	print_hundreds(unsigned int nbr, t_dict *dict)
{
	int	space;

	space = 0;
	if (nbr / 100)
	{
		print_tens(nbr / 100, dict);
		print_word(100, dict, 1);
		space = 1;
	}
	if (nbr % 100 > 0)
	{
		if (space)
			ft_putstr(" ");
		print_tens(nbr % 100, dict);
	}
}
