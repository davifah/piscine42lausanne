#include "load_dict.h"

char	*file_to_str(char *filename)
{
	int		fd;
	int		len;
	char	*buf;

	fd = open(filename, O_RDONLY);
	if (fd < 0)
		return (0);
	len = ft_filelen(fd);
	buf = malloc(sizeof(char) * len);
	if (!buf)
		return (0);
	close(fd);
	fd = open(filename, O_RDONLY);
	if (fd < 0)
		return (0);
	read(fd, buf, len);
	close(fd);
	return (buf);
}
