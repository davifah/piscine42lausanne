#include "ft.h"
#include "load_dict.h"
#include "nbr_to_word.h"

void	print_tens(unsigned int nbr, t_dict *dict)
{
	if (nbr <= 19)
		print_word(nbr, dict, 0);
	else
	{
		print_word((nbr / 10) * 10, dict, 0);
		if (nbr % 10 > 0)
			print_word(nbr % 10, dict, 1);
	}
}
