#include "ft.h"
#include "nbr_to_word.h"
#include "load_dict.h"

void	verify_thousands(unsigned int nbr, t_dict *dict, int *error)
{
	verify_hundreds(nbr / 1000, dict, error);
	if (nbr / 1000 && find_in_dict(1000, dict) < 0)
		(*error)++;
	if (nbr % 1000 > 0)
		verify_hundreds(nbr % 1000, dict, error);
}
