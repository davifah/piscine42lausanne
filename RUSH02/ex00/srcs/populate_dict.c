#include "load_dict.h"

int	manage_atoi_error(char *str, t_dict *dict, int *j);

t_dict	*populate_dict(char *str)
{
	t_dict	*dict_list;
	int		i;
	int		j;
	int		atoi_error;

	dict_list = malloc((sizeof(t_dict) * count_line(str)) + 1);
	if (!dict_list)
		return (0);
	i = -1;
	j = -1;
	while (str[++i])
	{
		if ((!i && str[i] != '\n') || (str[i] != '\n' && str[i - 1] == '\n'))
		{
			dict_list[++j].str = malloc(ft_linelen(&str[i]) * sizeof(char));
			if (!dict_list[j].str)
				return (0);
			atoi_error = j;
			if (manage_atoi_error(&str[i], &dict_list[j], &j))
				return (0);
		}
	}
	dict_list[j + (atoi_error - j)].str = 0;
	return (dict_list);
}

int	manage_atoi_error(char *str, t_dict *dict, int *j)
{
	int	atoi_error;

	atoi_error = parse_line(str, dict);
	if (atoi_error == -1)
		return (1);
	if (atoi_error == -2)
	{
		free(dict->str);
		*j = *j - 1;
	}
	return (0);
}
