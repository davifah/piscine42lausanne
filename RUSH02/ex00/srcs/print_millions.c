#include "ft.h"
#include "nbr_to_word.h"
#include "load_dict.h"

void	print_millions(unsigned int nbr, t_dict *dict)
{
	int	space;

	space = 0;
	print_hundreds(nbr / 1000000, dict);
	if (nbr / 1000000)
	{
		space = 1;
		print_word(1000000, dict, 1);
	}
	if (nbr % 1000000 > 0)
	{
		if (space)
			ft_putstr(" ");
		print_thousands(nbr % 1000000, dict);
	}
}
