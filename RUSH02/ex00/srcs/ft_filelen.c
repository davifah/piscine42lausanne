#include "load_dict.h"

int	ft_filelen(int fd)
{
	int		temp;
	int		len;
	char	ptr[100];

	temp = 1;
	len = 0;
	while (temp > 0)
	{
		temp = read(fd, ptr, 100);
		len += temp;
	}
	return (len - 1);
}
