#ifndef LOAD_DICT_H
# define LOAD_DICT_H
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include "ft.h"
# define DEFAULT_DICT "numbers.dict"
# define DICT_ERROR "Dict Error\n"

typedef struct s_dict
{
	unsigned int	nbr;
	char			*str;
}	t_dict;

int		ft_filelen(int fd);
int		ft_linelen(char *str);
char	*file_to_str(char *filename);
int		count_line(char *str);
int		parse_line(char *str, t_dict *dict);
t_dict	*verify_repetition(t_dict *dict_list);
t_dict	*verify_strings(t_dict *dict_list);
t_dict	*populate_dict(char *str);
t_dict	*load_dict(char *filename);
void	free_dict(t_dict *dict_list);
int		find_in_dict(unsigned int nbr, t_dict *dict);

#endif
