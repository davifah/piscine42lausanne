#ifndef NBR_TO_WORD_H
# define NBR_TO_WORD_H
# include "load_dict.h"

int		print_nbr_to_word(unsigned int nbr, char *filename);
int		print_word(unsigned int nbr, t_dict *dict, unsigned int space);
void	print_tens(unsigned int nbr, t_dict *dict);
void	print_hundreds(unsigned int nbr, t_dict *dict);
void	print_thousands(unsigned int nbr, t_dict *dict);
void	print_millions(unsigned int nbr, t_dict *dict);
void	print_billions(unsigned int nbr, t_dict *dict);
int		verify_dict(unsigned int nbr, t_dict *dict);
void	verify_tens(unsigned int nbr, t_dict *dict, int *error);
void	verify_hundreds(unsigned int nbr, t_dict *dict, int *error);
void	verify_thousands(unsigned int nbr, t_dict *dict, int *error);
void	verify_millions(unsigned int nbr, t_dict *dict, int *error);
void	verify_billions(unsigned int nbr, t_dict *dict, int *error);

#endif
