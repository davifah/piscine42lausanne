#ifndef FT_H
# define FT_H
# define ERROR "Error\n"
# include<unistd.h>

void	ft_putstr(char *str);
int		ft_strlen(char *str);
long	ft_atoi(char *str);
int		ft_power_ten(int n);
int		ft_str_is_numeric(char *str);
int		ft_str_is_printable(char *str);
int		ft_error(char *err);

#endif
