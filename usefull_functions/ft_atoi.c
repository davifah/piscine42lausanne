/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/11 11:02:27 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/11 17:18:36 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	get_int_start(char *str, int *negative);

int	isspace_condition(char c, int re, int had_negative);

int	ft_atoi(char *str)
{
	int				i;
	int				negative;
	int				level;
	unsigned int	n;

	negative = 1;
	level = 1;
	n = 0;
	i = get_int_start(str, &negative) + 1;
	while ('0' <= str[--i] && str[i] <= '9')
	{
		n += (str[i] - '0') * level;
		level *= 10;
	}
	return (n * negative);
}

int	get_int_start(char *str, int *negative)
{
	int	i;
	int	re;
	int	had_negative;
	int	is_done;

	i = -1;
	re = -1;
	had_negative = 0;
	is_done = 0;
	while (!is_done)
	{
		if (isspace_condition(str[++i], re, had_negative))
			;
		else if ((str[i] == '-' || str[i] == '+') && re < 0)
		{
			if (str[i] == '-')
				*negative *= -1;
			had_negative = 1;
		}
		else if ('0' <= str[i] && str[i] <= '9')
			re = i;
		else
			is_done = 1;
	}
	return (re);
}

int	isspace_condition(char c, int re, int had_negative)
{
	int	res;

	res = (9 <= c && c <= 13) || c == ' ';
	res *= re < 0;
	res *= !had_negative;
	return (res);
}
