#include "load_map.h"
#include "stdin_to_str.h"

t_map	*load_map(char *filename, int is_stdin)
{
	char	*file;
	t_map	*map;
	int		error;

	map = malloc(sizeof(t_map));
	if (!map)
		return (0);
	if (is_stdin)
		file = stdin_to_str(map);
	else
		file = file_to_str(filename);
	if (!file || count_line(file) < 2)
		return (0);
	if (is_stdin)
		error = 0;
	else
		error = parse_first_line(map, file);
	error += parse_map(map, file);
	free(file);
	if (error)
		return (0);
	return (map);
}
