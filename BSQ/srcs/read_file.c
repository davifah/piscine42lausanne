#include "load_map.h"

int	ft_filelen(int fd);

char	*file_to_str(char *filename)
{
	int		fd;
	int		i;
	int		len;
	char	*buf;

	fd = open(filename, O_RDONLY);
	if (fd < 0)
		return (0);
	len = ft_filelen(fd);
	buf = malloc(sizeof(char) * len + 1);
	if (!buf)
		return (0);
	i = -1;
	while (++i < len + 1)
		buf[i] = 0;
	close(fd);
	fd = open(filename, O_RDONLY);
	if (fd < 0)
		return (0);
	len = read(fd, buf, len);
	if (len < 0)
		return (0);
	close(fd);
	return (buf);
}

int	ft_filelen(int fd)
{
	int		temp;
	int		len;
	char	ptr[100];

	temp = 1;
	len = 0;
	while (temp > 0)
	{
		temp = read(fd, ptr, 100);
		len += temp;
	}
	return (len - 1);
}
