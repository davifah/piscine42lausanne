#include "stdin_to_str.h"

char	*list_to_str(t_char_list *item, char *str)
{
	if (item->next)
	{
		str[0] = item->c;
		list_to_str(item->next, &str[1]);
		return (str);
	}
	return (0);
}
