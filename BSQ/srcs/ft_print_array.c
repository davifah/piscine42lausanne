/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_array.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gaubert <gaubert@student.42lausanne.ch>    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/23 10:28:17 by gaubert           #+#    #+#             */
/*   Updated: 2021/08/25 12:50:21 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void	ft_print_int_array(int **matrix, int height, int length)
{
	int	i;
	int	j;

	i = -1;
	while (++i < height)
	{
		j = -1;
		while (++j < length)
		{
			ft_putnbr(matrix[i][j]);
		}
		write(1, "\n", 1);
	}
}

void	ft_print_char_array(char **matrix, int height, int length)
{
	int	i;
	int	j;

	i = -1;
	while (++i < height)
	{
		j = -1;
		while (++j < length)
		{
			write(1, &matrix[j][i], 1);
		}
		write(1, "\n", 1);
	}
}
