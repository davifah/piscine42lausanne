#include "stdin_to_str.h"

t_char_list	*list_append(t_char_list *item, char c_ap)
{
	if (item->next == NULL)
	{
		item->next = malloc(sizeof(t_char_list));
		if (!item->next)
			return (0);
		item->c = c_ap;
		item->next->next = 0;
		return (item->next);
	}
	return (list_append(item->next, c_ap));
}
