#include "ft.h"

int	count_nbrs(char *str);

long	ft_atoi(char *str)
{
	int		i;
	int		size;
	long	n;

	i = -1;
	n = 0;
	size = count_nbrs(str);
	if (size > 10)
		return (-2);
	while (str[++i] == '0')
		;
	i--;
	while ('0' <= str[++i] && str[i] <= '9')
		n += (long)(str[i] - '0')*(ft_power_ten(--size));
	if (!i)
		return (-1);
	if (n <= 4294967295 && n >= 0)
		return (n);
	return (-2);
}

int	count_nbrs(char *str)
{
	int	i;
	int	counter;

	i = -1;
	counter = 0;
	while (str[++i] == '0')
		;
	i--;
	while ('0' <= str[++i] && str[i] <= '9')
		counter++;
	return (counter);
}
