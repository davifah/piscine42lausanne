#include "stdin_to_str.h"

int		do_parseline(t_map *map, t_char_list *list_start);
char	*free_list(t_char_list *start, char *str);
void	free_list_rec(t_char_list *item);

char	*stdin_to_str(t_map *map)
{
	t_char_list	*list_start;
	t_char_list	*last;
	char		c;
	char		*str;
	int			counter;

	list_start = malloc(sizeof(t_char_list));
	last = list_start;
	map->height = 0;
	counter = -1;
	while ((long)counter < (long)map->height)
	{
		read(0, &c, 1);
		if (!last)
			return (0);
		if ((long)counter < (long)map->height)
			last = list_append(last, c);
		if (c == '\n' && !++counter)
			if (do_parseline(map, list_start))
				return (0);
	}
	str = malloc(sizeof(char) * (list_len(list_start) + 1));
	if (!str || (int)list_to_str(list_start, str) * 0)
		return (0);
	return (free_list(list_start, str));
}

int	do_parseline(t_map *map, t_char_list *list_start)
{
	char	*str;

	str = malloc(sizeof(char) * (list_len(list_start) + 1));
	if (!str)
		return (1);
	list_to_str(list_start, str);
	if (parse_first_line(map, str))
		return (1);
	free(str);
	return (0);
}

char	*free_list(t_char_list *start, char *str)
{
	free_list_rec(start);
	free(start);
	return (str);
}

void	free_list_rec(t_char_list *item)
{
	if (item->next)
	{
		free_list_rec(item->next);
		free(item->next);
	}
}
