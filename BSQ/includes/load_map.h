#ifndef LOAD_MAP_H
# define LOAD_MAP_H
# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct s_map
{
	unsigned int	height;
	unsigned int	width;
	char			**matrix;
	char			empty;
	char			obstacle;
	char			full;
	int				maxval;
	unsigned int	maxj;
	unsigned int	maxi;
}	t_map;

char	*file_to_str(char *filename);
int		ft_linelen(char *str);
int		count_line(char *str);
int		parse_first_line(t_map *map, char *str);
int		parse_map(t_map *map, char *str);
void	free_map(t_map *map);
t_map	*load_map(char *filename, int is_stdin);

#endif
