#include <unistd.h>

void	ft_putchar(char c);

void	fct(int x, char first_c, char middle_c, char last_c);

void	rush(int x, int y)	
{
	int		i;
	int		j;

	i = 0;
	if (x < 0 || y < 0)
		return ;
	while (i != y)
	{
		j = 0;
		if (i == 0)
		{	
			fct(x, '/', '*', '\\');
		}
		else if (i < y - 1)
		{	
			fct(x, '*', ' ', '*');
		}
		else if (i == y - 1)
		{	
			fct(x, '\\', '*', '/');
		}
		i++;
		ft_putchar('\n');
	}
}

void	fct(int x, char first_c, char middle_c, char last_c)
{
	int		j;

	j = 0;
	while (j <= x - 1)
	{
		if (j == 0)
			ft_putchar (first_c);
		else if (j == x - 1)
			ft_putchar (last_c);
		else
			ft_putchar (middle_c);
		j++;
	}
}
