/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/05 18:16:51 by dfarhi            #+#    #+#             */
/*   Updated: 2021/08/05 18:37:32 by dfarhi           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

int	ft_is_sorted(int *tab, int size);

// BUBBLE SORT ALGORITHM
void	ft_sort_int_tab(int *tab, int size)
{
	int	i;
	int	temp;

	while (!ft_is_sorted(tab, size))
	{
		i = 0;
		while (++i < size)
		{
			if (tab[i] < tab[i - 1])
			{
				temp = tab[i];
				tab[i] = tab[i - 1];
				tab[i - 1] = temp;
			}
		}	
	}
}

int	ft_is_sorted(int *tab, int size)
{
	int	i;
	int	last;

	i = -1;
	last = -2147483648;
	while (++i < size)
	{
		if (last > tab[i])
			return (0);
		last = tab[i];
	}
	return (1);
}
